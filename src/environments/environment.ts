// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase:{
    apiKey: "AIzaSyDf5o4QnxAZ_Tx_xBgra6SgP7qK0xxW1sI",
    authDomain: "movies-fa232.firebaseapp.com",
    databaseURL: "https://movies-fa232.firebaseio.com",
    projectId: "movies-fa232",
    storageBucket: "movies-fa232.appspot.com",
    messagingSenderId: "407922996083"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.